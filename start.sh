#!/bin/bash

set -eu

mkdir -p /app/data/runtime /app/data/config

if [[ ! -f /app/data/config/alertmanager.yml ]]; then
    echo "=> Creating config file on first run"
    cp /app/code/alertmanager.yml /app/data/config/alertmanager.yml
fi

chown -R cloudron:cloudron /app/data

echo "=> Starting Prometheus Alertmanager"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/alertmanager --config.file=/app/data/config/alertmanager.yml --storage.path=/app/data/runtime
